variable "prefix" {
  default = "sbr"
  type    = string
}

variable "project" {
  type    = string
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "sebastiaan.brozius@solvinity.com"
  type    = string
}

variable "db_username" {
  description = "Username for the RDS PostgreSQL Instance"
}

variable "db_password" {
  description = "Password for the RDS PostgreSQL Instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for api"
  default     = "165461416712.dkr.ecr.eu-central-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "165461416712.dkr.ecr.eu-central-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "cloud-tinkering.nl"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
